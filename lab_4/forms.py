from django.forms import ModelForm, Textarea, TextInput, widgets
from lab_2.models import Note

class InputShortText(TextInput):
    input_type = 'text'

class InputLongText(Textarea):
    input_type = 'text'

class NoteForm(ModelForm):
    class Meta:
        model = Note
        fields = "__all__"
        widgets = {'toName': InputShortText(attrs={'placeholder': 'Enter the recipient name'}),
                   'fromName': InputShortText(attrs={'placeholder': 'Enter your name'}), 'title': InputShortText(attrs={'placeholder': 'Enter the title of the note'}), 'message': InputLongText(attrs={'placeholder': 'Enter your messages'})}
