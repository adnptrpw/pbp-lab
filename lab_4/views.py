from django.http import response
from django.shortcuts import render
from lab_2.models import Note
from lab_4.forms import NoteForm


def index(request):
    notes = Note.objects.all()
    response = {'notes': notes}
    return render(request, 'lab4_index.html', response)


def add_note(request):
    form = NoteForm(request.POST or None)

    if (form.is_valid() and request.method == 'POST'):
        form.save()
        return response.HttpResponseRedirect('/lab-4')
    else:
        form = NoteForm()

    return render(request, 'lab4_form.html', {'form': form})

def note_list(request):
    notes = Note.objects.all()
    response = {'notes': notes}
    return render(request, 'lab4_note_list.html', response)
