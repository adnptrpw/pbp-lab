# Lab 2: Data Delivery Using HTML, XML, and JSON

Andini Putri Pramudya Wardani - 2006527222<br>
PBP A

## 1. Apakah perbedaan antara JSON dan XML?

JSON merupakan format yang digunakan untuk menyimpan informasi dengan lebih terorganisir dan mudah diakses.<br>
Sedangkan XML merupakan bahasa markup yang dirancang untuk menyimpan data.

## 2. Apakah perbedaan antara HTML dan XML?

XML berfokus pada transfer data sedangkan HTML berfokus pada penyajian data.

## Referensi

- [Apa Perbedaan JSON Dan XML?](https://www.monitorteknologi.com/perbedaan-json-dan-xml/)
- [Apa Saja Perbedaan XML dan HTML](https://blogs.masterweb.com/perbedaan-xml-dan-html/#:~:text=XML%20adalah%20singkatan%20dari%20eXtensible,HTML%20difokuskan%20pada%20penyajian%20data.)
