from django.db import models

class Note(models.Model):
    toName = models.CharField(max_length=30)
    fromName = models.CharField(max_length=30)
    title = models.CharField(max_length=120);
    message = models.TextField();
    
